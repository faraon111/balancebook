var profitDateField = document.getElementById("profitDateField");
var profitMoneyField = document.getElementById("profitMoneyField");
var profitSourceField = document.getElementById("profitSourceField");
var profitCardsDiv = document.getElementById("profitCardsDiv");

var expenseDateField = document.getElementById("expenseDateField");
var expenseMoneyField = document.getElementById("expenseMoneyField");
var expenseSourceField = document.getElementById("expenseSourceField");
var expenseCardsDiv = document.getElementById("expenseCardsDiv");

var profitDiv = document.getElementById("profitDiv");
var expenseDiv = document.getElementById("expenseDiv");
var balanceDiv = document.getElementById("balanceDiv");

function Transaction(date,money,source)
{
    this.date=date;
    this.money=money;
    this.source=source;
}

var arrayProfits=[];
var arrayExpenses=[];

function CalculateBalance()
{
    var profit = ProfitGetAllMoney();
    var expense = ExpenseGetAllMoney();
    var balance = profit-expense;

    profitDiv.innerHTML = "Доход<br/>" + profit;
    expenseDiv.innerHTML = "Расход<br/>" + expense;
    balanceDiv.innerHTML = "Баланс<br/>" + balance;


}

/*======================START PROFIT=========================*/
function ProfitAddButtonClick()
{

    if(profitDateField.value == "" || profitMoneyField.value == "" || profitSourceField.value == "" )
    {
        alert("Заполните все поля");
        return;
    }
    if(parseInt(profitMoneyField.value)<0)
    {
        alert("Введите сумму дохода больше 0");
        return;
    }

    var date = profitDateField.value;
    var money=parseInt(profitMoneyField.value);
    var source=profitSourceField.value;

    arrayProfits.push(new Transaction(date,money,source));
    ProfitRefreshCardsDiv();
    CalculateBalance();

    profitDateField.value="";
    profitMoneyField.value="";
    profitSourceField.value="";
}

function ProfitRefreshCardsDiv()
{
    var HTMLcode="";
    for (var i=0;i<arrayProfits.length;i++)
    {
        HTMLcode+="<div class='profitCard'>";

        HTMLcode+="<p> <input type='date' value='"+arrayProfits[i].date+"'  readonly class='profitCardItem' id='profitCardDateField"+i+"'></p>";
        HTMLcode+="<p> <input type='text' value='"+arrayProfits[i].money+"'  readonly class='profitCardItem' id='profitCardMoneyField"+i+"'></p>";
        HTMLcode+="<p> <input type='text' value='"+arrayProfits[i].source+"'  readonly class='profitCardItem' id='profitCardSourceField"+i+"'></p>";

        HTMLcode+="<p><input type='button' class='profitCardButton' value='Редактировать' id='profitEditButton"+i+"' onclick='ProfitEditButtonClick("+i+")'></p>";
        HTMLcode+="<p><input type='button' class='profitCardButton' value='Применить' onclick='ProfitApplyButtonClick("+i+")' id='profitApplyButton"+i+"' hidden></p>";
        HTMLcode+="<p><input type='button' class='profitCardButton' value='Удалить' onclick='ProfitDeleteCardClick("+i+")'></p>";

        HTMLcode+="</div>";
    }
    profitCardsDiv.innerHTML=HTMLcode;
}
function ProfitDeleteCardClick(index)
{
    arrayProfits.splice(index,1);
    ProfitRefreshCardsDiv();
    CalculateBalance();

}
function ProfitEditButtonClick(index)
{
    document.getElementById("profitCardDateField"+index).readOnly=false;
    document.getElementById("profitCardMoneyField"+index).readOnly=false;
    document.getElementById("profitCardSourceField"+index).readOnly=false;

    document.getElementById("profitEditButton"+index).hidden=true;
    document.getElementById("profitApplyButton"+index).hidden=false;
}
function ProfitApplyButtonClick (index)
{
    var date = document.getElementById("profitCardDateField"+index).value;
    var money = parseInt(document.getElementById("profitCardMoneyField"+index).value);
    var source = document.getElementById("profitCardSourceField"+index).value;

    arrayProfits[index]=new Transaction(date,money,source);
    ProfitRefreshCardsDiv();
    CalculateBalance();
}

function ProfitGetAllMoney()
{
    var sum=0;
    for(var i=0; i<arrayProfits.length;i++)
    {
        sum+=arrayProfits[i].money;
    }
    return sum;
}
/*======================FINISH PROFIT=========================*/




/*======================START EXPENSE=========================*/
function ExpenseAddButtonClick()
{
    if(expenseDateField.value == "" || expenseMoneyField.value == "" || expenseSourceField.value == "" )
    {
        alert("Заполните все поля");
        return;
    }
    if(parseInt(expenseMoneyField.value)<0)
    {
        alert("Введите сумму расхода больше 0");
        return;
    }
    
    var date = expenseDateField.value;
    var money=parseInt(expenseMoneyField.value);
    var source=expenseSourceField.value;

    arrayExpenses.push(new Transaction(date,money,source));
    ExpenseRefreshCardsDiv();
    CalculateBalance();

    expenseDateField.value="";
    expenseMoneyField.value="";
    expenseSourceField.value="";
}
function ExpenseRefreshCardsDiv()
{
    var HTMLcode="";
    for (var i=0;i<arrayExpenses.length;i++)
    {
        HTMLcode+="<div class='expenseCard'>";

        HTMLcode+="<p> <input type='date' value='"+arrayExpenses[i].date+"'  readonly class='expenseCardItem' id='expenseCardDateField"+i+"'></p>";
        HTMLcode+="<p> <input type='text' value='"+arrayExpenses[i].money+"'  readonly class='expenseCardItem' id='expenseCardMoneyField"+i+"'></p>";
        HTMLcode+="<p> <input type='text' value='"+arrayExpenses[i].source+"'  readonly class='expenseCardItem' id='expenseCardSourceField"+i+"'></p>";

        HTMLcode+="<p><input type='button' class='expenseCardButton' value='Редактировать' id='expenseEditButton"+i+"' onclick='ExpenseEditButtonClick("+i+")'></p>";
        HTMLcode+="<p><input type='button' class='expenseCardButton' value='Применить' onclick='ExpenseApplyButtonClick("+i+")' id='expenseApplyButton"+i+"' hidden></p>";
        HTMLcode+="<p><input type='button' class='expenseCardButton' value='Удалить' onclick='ExpenseDeleteCardClick("+i+")'></p>";

        HTMLcode+="</div>";
    }
    expenseCardsDiv.innerHTML=HTMLcode;
}

function ExpenseDeleteCardClick(index)
{
    arrayExpenses.splice(index,1);
    ExpenseRefreshCardsDiv();
    CalculateBalance();
}

function ExpenseEditButtonClick(index)
{
    document.getElementById("expenseCardDateField"+index).readOnly=false;
    document.getElementById("expenseCardMoneyField"+index).readOnly=false;
    document.getElementById("expenseCardSourceField"+index).readOnly=false;

    document.getElementById("expenseEditButton"+index).hidden=true;
    document.getElementById("expenseApplyButton"+index).hidden=false;
}

function ExpenseApplyButtonClick (index)
{
    var date = document.getElementById("expenseCardDateField"+index).value;
    var money = parseInt(document.getElementById("expenseCardMoneyField"+index).value);
    var source = document.getElementById("expenseCardSourceField"+index).value;

    arrayExpenses[index]=new Transaction(date,money,source);
    ExpenseRefreshCardsDiv();
    CalculateBalance();
}

function ExpenseGetAllMoney()
{
    var sum=0;
    for(var i=0; i<arrayExpenses.length;i++)
    {
        sum+=arrayExpenses[i].money;
    }
    return sum;
}
/*======================FINISH EXPENSE=========================*/
